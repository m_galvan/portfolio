# Our awesome calendar



### Version

Version 1.0.0.



### Présentation

Our awesome calendar est un projet exercice réalisé en groupe. 

Nous devions utiliser la librairie moment js afin d'apprendre à manipuler les dates.



<img src="images/calendar.png">





### Technologies 

Plusieurs technologies nous ont accompagné dans ce projet :

* HTML
* CSS
* Bootstrap
* JavaScript
* Moment js
* Webpack





### Utilisation

Des flèches sont à la disposition des utilisateurs pour naviguer de mois en mois (possibilité d'utiliser celle du clavier). 


### A venir...

A l'avenir le calendrier subira quelques améliorations.			 

 * Possibilité d'ajouter des évènements
 * Catégoriser ses évènements