const moment = require('moment');
moment.locale('fr');

export class Event{
    constructor(date, hour, description){
        this.date = date;
        this.hour = hour;
        this.description = description;
    }
    add(tabArg) {
            let td = document.querySelector('td');
            td.innerHTML = '';

            for (const events of tabArg) {
                let p = document.createElement('p');
                td.appendChild(p);
                let days = document.querySelector('.day-1');
                
                p.innerHTML = events.hour + '<br>' + events.description;
                days.appendChild(p);
            }
    }
    edit() {

    }
    remove() {

    }
    notif() {
        
    }
}